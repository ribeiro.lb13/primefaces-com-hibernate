package managedBean;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import dao.DaoTelefone;
import dao.DaoUsuario;
import model.TelefoneUser;
import model.UsuarioPessoa;

@ManagedBean(name = "telefoneManagedBean")
@ViewScoped
public class TelefoneManagedBean {
	private UsuarioPessoa user = new UsuarioPessoa();
	private TelefoneUser tel = new TelefoneUser();

	private DaoUsuario<UsuarioPessoa> daoUser = new DaoUsuario<UsuarioPessoa>();
	private DaoTelefone<TelefoneUser> daoTelefone = new DaoTelefone<TelefoneUser>();

	public DaoUsuario<UsuarioPessoa> getDaoUser() {
		return daoUser;
	}

	public void setDaoUser(DaoUsuario<UsuarioPessoa> daoUser) {
		this.daoUser = daoUser;
	}

	public DaoTelefone<TelefoneUser> getDaoTelefone() {
		return daoTelefone;
	}

	public void setDaoTelefone(DaoTelefone<TelefoneUser> daoTelefone) {
		this.daoTelefone = daoTelefone;
	}

	@PostConstruct
	public void init() {
		String coduser = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("codigouser");
		user = daoUser.pesquisar(Long.parseLong(coduser), UsuarioPessoa.class);
	}

	public String salvar() {
		tel.setUsuarioPessoa(user);
		daoTelefone.salvar(tel);
		user = daoUser.pesquisar((user.getId()), UsuarioPessoa.class);
		//tel = new TelefoneUser();
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Informação: ", "Telefone Salvo!"));
		return "";
	}

	public String removeTelefone() {

		daoTelefone.deletarPorId(tel);
		user = daoUser.pesquisar((user.getId()), UsuarioPessoa.class);
		tel = new TelefoneUser();
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Informação: ", "Telefone Removido!"));

		return "";

	}

	public UsuarioPessoa getUser() {
		return user;
	}

	public void setUser(UsuarioPessoa user) {
		this.user = user;
	}

	public TelefoneUser getTel() {
		return tel;
	}

	public void setTel(TelefoneUser tel) {
		this.tel = tel;
	}

}
