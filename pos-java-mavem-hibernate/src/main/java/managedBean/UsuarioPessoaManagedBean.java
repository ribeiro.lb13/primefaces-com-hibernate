package managedBean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.apache.tomcat.util.codec.binary.Base64;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.google.gson.Gson;

import dao.DaoEmail;
import dao.DaoUsuario;
import datatablelazy.LazyDataTableModelUserPessoa;
import model.UsuarioPessoa;
import model.emailUser;

@javax.faces.bean.ManagedBean(name = "usuarioPessoaManagedBean")
@ViewScoped
public class UsuarioPessoaManagedBean {

	private UsuarioPessoa pessoa = new UsuarioPessoa();
	private LazyDataTableModelUserPessoa<UsuarioPessoa> listPessoa = new LazyDataTableModelUserPessoa<UsuarioPessoa>();
	private DaoUsuario<UsuarioPessoa> daoGeneric = new DaoUsuario<UsuarioPessoa>();
	private BarChartModel bar = new BarChartModel();
	private emailUser email = new emailUser();
	private DaoEmail<emailUser> daoEmail = new DaoEmail<emailUser>();
	private String campoPesquisa;

	public void pesquisarNome() {
		listPessoa.pesquisa(campoPesquisa);
		montarGrafico();
	}

	public void addEmail() {
		email.setUsuarioPessoa(pessoa);
		email = daoEmail.updateMerge(email);
		pessoa.getEmailUsers().add(email);
		email = new emailUser();
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Informação: ", "E-mail salvo com sucesso!"));
	}

	public void removerEmail() {
		String codigoemail = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("codigoemail");

		emailUser remover = new emailUser();
		remover.setId(Long.parseLong(codigoemail));
		daoEmail.deletarPorId(remover);
		pessoa.getEmailUsers().remove(remover);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Informação: ", "E-mail removido com sucesso!"));
	}

	public emailUser getEmail() {
		return email;
	}

	public void setEmail(emailUser email) {
		this.email = email;
	}

	public BarChartModel getBar() {
		return bar;
	}

	public void setBar(BarChartModel bar) {
		this.bar = bar;
	}

	@PostConstruct
	public void init() {
		listPessoa.load(0, 5, null, null, null);

		montarGrafico();

	}

	private void montarGrafico() {
		bar = new BarChartModel();
		ChartSeries userSalario = new ChartSeries("Salário dos Usuários"); /* Grupo de funcionários */
		userSalario.setLabel("USers");

		for (UsuarioPessoa usuarioPessoa : listPessoa.list) { /* add salário para para o grupo */
			userSalario.set(usuarioPessoa.getNome(), usuarioPessoa.getSalario()); /* add salarios */
		}
		bar.addSeries(userSalario); /* adiciona o grupo no barmodel */
		bar.setTitle("Gráfico de salários");
	}

	public String salvar() {
		daoGeneric.salvar(pessoa);
		listPessoa.list.add(pessoa);
		init();
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, "Informação: ", "Salvo com sucesso!"));
		return "";
	}

	public String novo() {
		pessoa = new UsuarioPessoa();
		return "";
	}

	public String remover() {
		try {

			daoGeneric.removerUsuario(pessoa);
			listPessoa.list.remove(pessoa);
			pessoa = new UsuarioPessoa();
			init();
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Informação: ", "Removido com sucesso!"));
		} catch (Exception e) {
			if (e.getCause() instanceof org.hibernate.exception.ConstraintViolationException) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Informação: ", "Existem telefones para o usuario!"));
			} else {
				e.printStackTrace();
			}

		}
		return "";
	}

	public void pesquisaCep(AjaxBehaviorEvent e2) {
		try {
			URL url = new URL("https://viacep.com.br/ws/" + pessoa.getCep() + "/json/");
			URLConnection connection = url.openConnection();
			InputStream is = connection.getInputStream();
			BufferedReader br = new BufferedReader(new java.io.InputStreamReader(is, "UTF-8"));

			String cep = "";
			StringBuilder jasonCep = new StringBuilder();
			while ((cep = br.readLine()) != null) {
				jasonCep.append(cep);
			}

			UsuarioPessoa gsonAux = new Gson().fromJson(jasonCep.toString(), UsuarioPessoa.class);
			pessoa.setCep(gsonAux.getCep());
			pessoa.setLogradouro(gsonAux.getLogradouro());
			pessoa.setComplemento(gsonAux.getComplemento());
			pessoa.setBairro(gsonAux.getBairro());
			pessoa.setLocalidade(gsonAux.getLocalidade());
			pessoa.setUf(gsonAux.getUf());
			pessoa.setIbge(gsonAux.getIbge());
			pessoa.setGia(gsonAux.getGia());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public LazyDataTableModelUserPessoa<UsuarioPessoa> getListPessoa() {
		montarGrafico();
		return listPessoa;
	}

	public UsuarioPessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(UsuarioPessoa pessoa) {
		this.pessoa = pessoa;
	}

	public String getCampoPesquisa() {
		return campoPesquisa;
	}

	public void setCampoPesquisa(String campoPesquisa) {
		this.campoPesquisa = campoPesquisa;
	}

	public void upload(FileUploadEvent image) {
		String imagem = "data:image/png;base64," + DatatypeConverter.printBase64Binary(image.getFile().getContents());
		pessoa.setImagem(imagem);

	}

	public void download() throws IOException {
		String fileDownload = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get("fileUpload");

		UsuarioPessoa pessoa = daoGeneric.pesquisar(Long.parseLong(fileDownload), UsuarioPessoa.class);

		byte[] imagem = Base64.decodeBase64(pessoa.getImagem().split("\\,")[1]);

		HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
				.getResponse();

		response.addHeader("Content-Disposition", "attachment; filename=download.png");
		response.setContentType("application/octet-stream");
		response.setContentLength(imagem.length);
		response.getOutputStream().write(imagem);
		response.getOutputStream().flush();
		FacesContext.getCurrentInstance().responseComplete();
	}

}
