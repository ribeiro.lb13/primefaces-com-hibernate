package dao;

import java.util.List;

import javax.persistence.Query;

import datatablelazy.LazyDataTableModelUserPessoa;
import model.UsuarioPessoa;

public class DaoUsuario<E> extends DaoGeneric<UsuarioPessoa> {

	public void removerUsuario(UsuarioPessoa usuario) {
		
		
		getEntityManager().getTransaction().begin();		

		//getEntityManager().remove(usuario);
		getEntityManager().remove(getEntityManager().getReference(UsuarioPessoa.class, usuario.getId()));

		getEntityManager().getTransaction().commit();

		super.deletarPorId(usuario);
	}

	public List<UsuarioPessoa> pesquisar(String campoPesquisa) {
		return getEntityManager().createQuery("from UsuarioPessoa where nome like '%" + campoPesquisa + "%'", UsuarioPessoa.class).getResultList();
	}


}
